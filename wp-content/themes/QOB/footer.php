<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>
<footer id="footer">
    <div class="outer">
        <?php
        echo get_sidebar('footer');
        ?>
        <div class="inner-1">
            <div class="container">
                <p class="pull-left copy">© Copyright QOB Cosmetics 2014 </p>
                <div class="pull-right"><i class="glyphicon glyphicon-earphone"></i>
                    <p>HOTLINE: <a href="tel:083. 3555 999" title="phone"><span>083. 3555 999</span></a></p>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>
<script src="<?php echo get_template_directory_uri(); ?>/js/l10n.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/libs.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/script.js"></script>
<script>var addthis_config = {"data_track_addressbar": false}</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52329f023629ee45"></script>
<!--[if lt IE 8]><p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p><![endif]-->
<noscript>JavaScript is off. Please enable to view full site.</noscript>
<?php wp_footer(); ?>
</body>
</html>