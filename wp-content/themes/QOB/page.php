<?php
get_header();
global $cfs;
?>
<main id="main">
    <div class="container">
        <div class="highlight">
            <?php
            while (have_posts()) : the_post();
                $image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'page_left');
                $desc_left = $cfs->get('destination', get_the_ID());
                $image_right = wp_get_attachment_image_src($cfs->get('image', get_the_ID()), 'page_right');
                ?>
                <h2 class="title"><span><?php the_title(); ?></span><span class="border"></span></h2>
                <div class="row inner">
                    <div class="col-md-6 thumb"><img src="<?php echo $image[0]; ?>" alt=""></div>
                    <div class="col-md-6 desc">
                        <?php the_content(); ?>  
                    </div>
                    <div class="col-md-9 desc">
                        <?php echo $desc_left ?>
                    </div>
                    <div class="col-md-3 thumb"><img src="<?php echo $image_right[0] ?>" alt=""></div>
                </div>
            <?php endwhile; ?>
        </div>
    </div>
</main>
<?php get_footer(); ?>