<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
get_header();

global $cfs;
$products = get_list_product_sanphammoi();
?>
<div class="inner">
    <main id="main">
        <div class="container">
            <div class="highlight">
                <div class="block product-block">
                    <h2 class="title"><span><?php _e('SẢN PHẨM MỚI', THEMENAME); ?></span><span class="border"></span></h2>
                    <div class="inner">
                        <?php
                        foreach ($products as $product) :
                            $price = $cfs->get('price', $product->ID);
                            $image_product = $cfs->get('image_product', $product->ID);
                            $image = wp_get_attachment_image_src($image_product[0]["image"], 'sanphammoi');
                            ?>
                            <div class="col-sm-6 col-md-4">
                                <div class="thumbnail">
                                    <a href="<?php echo get_permalink($product->ID); ?>" title="San pham 1">
                                        <img src="<?php echo $image[0]; ?>" alt="<?php echo $product->post_title ?>"/>
                                        <div class="caption">
                                            <h3><?php echo $product->post_title ?></h3>
                                            <p class="price">$ <?php echo $price ?> vnđ</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <?php
                        endforeach;
                        ?>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6">
                    <?php $posts_new = get_post_tintuc(); ?>
                    <h2 class="title-1"><?php _e('Tin mới nhất', THEMENAME); ?></h2>
                    <div class="block-1">
                        <?php
                        foreach ($posts_new as $new) :
                            $image = wp_get_attachment_image_src(get_post_thumbnail_id($new->ID), 'tintucnew');
                            ?>
                            <div class="media">
                                <a href="<?php echo get_permalink($new->ID); ?>" title="<?php echo $new->post_title ?>">
                                    <h3 class="heading"><?php echo $new->post_title ?></h3>
                                    <img src="<?php echo $image[0]; ?>" alt="<?php echo $new->post_title ?>" class="pull-left media-object"/>
                                    <div class="media-body">
                                        <div class="desc">
                                            <p><?php echo wp_trim_words($new->post_content, 30, '...'); ?></p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <?php
                        endforeach;
                        ?>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6">
                    <h2 class="title-1"><?php _e('Cảm nhận khách hàng', THEMENAME); ?></h2>
                    <div class="block-1">
                        <div class="inner">
                            <?php $camnhankhachhang = get_post_customer_experience(); ?>
                            <h3><a href="<?php echo get_permalink($camnhankhachhang[0]->ID); ?>" title="<?php echo $camnhankhachhang[0]->post_title ?>"><?php echo $camnhankhachhang[0]->post_title ?></a></h3>
                            <div class="caption"></div>
                            <p><strong><?php echo $cfs->get('short_descrition', $camnhankhachhang[0]->ID); ?></strong></p>
                            <?php echo apply_filters('the_content', $cfs->get('youtube', $camnhankhachhang[0]->ID) . '&width=460&height=330'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
<?php get_footer(); ?>