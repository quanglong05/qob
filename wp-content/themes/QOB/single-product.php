<?php
get_header();
global $cfs;
?>
<div class="inner">
    <main id="main">
        <div class="container">
            <div class="highlight">
                <?php
                while (have_posts()) : the_post();
                    $image_product = $cfs->get('image_product', get_the_ID());
                    $price_product = $cfs->get('price', get_the_ID());
                    $the_tich_product = $cfs->get('the_tich', get_the_ID());
                    $xuat_xu_product = $cfs->get('xuat_xu', get_the_ID());
                    $giay_phep_product = $cfs->get('giay_phep', get_the_ID());
                    $han_su_dung_product = $cfs->get('han_su_dung', get_the_ID());
                    $giao_hang_product = $cfs->get('giao_hang', get_the_ID());
                    $tinh_trang_product = $cfs->get('tinh_trang', get_the_ID());
                    $thong_tin_san_pham_product = $cfs->get('thong_tin_san_pham', get_the_ID());
                    $thong_so_ky_thuat_product = $cfs->get('thong_so_ky_thuat', get_the_ID());
                    $support_product = $cfs->get('support', get_the_ID());
                    ?>
                    <div class="block product-detail">
                        <h2 class="title"><span><?php _e('Chi tiết sản phẩm', THEMENAME); ?></span><span class="border"></span></h2>
                        <div class="inner">
                            <div class="row">
                                <div class="col-md-7 col-sm-6">
                                    <div id="slide" data-ride="carousel" class="carousel slide">
                                        <div class="carousel-inner">
                                            <?php
                                            foreach ($image_product as $key_product => $product_image) :
                                                $image = wp_get_attachment_image_src($product_image["image"], 'product_detail');
                                                ?>
                                                <div class="item <?php echo $key_product == 0 ? 'active' : '' ?>"><img src="<?php echo $image[0]; ?>" alt="img-13"></div>
                                                <?php
                                            endforeach;
                                            ?>
                                        </div>
                                        <ol class="carousel-indicators">
                                            <?php
                                            foreach ($image_product as $key_product => $product_image) :
                                                $image = wp_get_attachment_image_src($product_image["image"], 'product_detail');
                                                ?>
                                                <li data-target="#slide" data-slide-to="<?php echo $key_product ?>" <?php echo $key_product == 0 ? 'class="active"' : '' ?>><img src="<?php echo $image[0]; ?>" alt="img-13"></li>
                                                <?php
                                            endforeach;
                                            ?>
                                        </ol>
                                        <a href="#slide" role="button" data-slide="prev" class="left carousel-control"><span></span></a>
                                        <a href="#slide" role="button" data-slide="next" class="right carousel-control"><span></span></a>
                                    </div>
                                </div>
                                <div class="col-md-5 col-sm-6">
                                    <div class="block-infor">
                                        <h3><?php the_title(); ?></h3>
                                        <div class="desc">
                                            <?php the_content(); ?>
                                            <ul class="list-unstyled">
                                                <li>
                                                    <h4><?php _e('Giá bán', THEMENAME) ?>:</h4>
                                                    <p class="info"><?php echo $price_product ?> vnđ </p>
                                                </li>
                                                <li>
                                                    <h4><?php _e('Mã SP', THEMENAME) ?>:</h4>
                                                    <p class="info">QOB<?php echo get_the_ID(); ?></p>
                                                </li>
                                                <li>
                                                    <h4><?php _e('Xuất xứ', THEMENAME) ?>:</h4>
                                                    <p class="info"><?php echo $xuat_xu_product ?></p>
                                                </li>
                                                <li>
                                                    <h4><?php _e('Thể tích', THEMENAME) ?>:</h4>
                                                    <p class="info"><?php echo $the_tich_product ?></p>
                                                </li>
                                                <li>
                                                    <h4><?php _e('Giấy phép', THEMENAME) ?>:</h4>
                                                    <p class="info"><?php echo $giay_phep_product ?></p>
                                                </li>
                                                <li>
                                                    <h4><?php _e('Tình trạng', THEMENAME) ?>:</h4>
                                                    <p class="info"><?php echo $tinh_trang_product ?></p>
                                                </li>
                                                <li>
                                                    <h4><?php _e('Hạn sử dụng', THEMENAME) ?>:</h4>
                                                    <p class="info"><?php echo $han_su_dung_product ?></p>
                                                </li>
                                                <li>
                                                    <h4><?php _e('Giao hàng', THEMENAME) ?>:</h4>
                                                    <p class="info"><?php echo $giao_hang_product ?></p>
                                                </li>
                                                <li>
                                                    <div class="addthis_native_toolbox"></div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 tab-block">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#tab-1" data-toggle="tab" title="<?php _e('Thông tin sản phẩm', THEMENAME) ?>"><?php _e('Thông tin sản phẩm', THEMENAME) ?></a></li>
                                        <li><a href="#tab-2" data-toggle="tab" title="<?php _e('Hướng dẫn sử dụng', THEMENAME) ?>"><?php _e('Hướng dẫn sử dụng', THEMENAME) ?></a></li>
                                        <li><a href="#tab-3" data-toggle="tab" title="<?php _e('Cảm nhận khách hàng', THEMENAME) ?>"><?php _e('Cảm nhận khách hàng', THEMENAME) ?></a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div id="tab-1" class="tab-pane active fade in">
                                            <div class="desc">
                                                <?php echo $thong_tin_san_pham_product ?>
                                            </div>
                                        </div>
                                        <div id="tab-2" class="tab-pane fade">
                                            <div class="desc">
                                                <?php echo $thong_so_ky_thuat_product ?>
                                            </div>
                                        </div>
                                        <div id="tab-3" class="tab-pane fade">
                                            <div class="desc">
                                                <?php echo $support_product ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 comment-block">
                                    <?php comments_template(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    $category_list = implode(wp_get_post_terms(get_the_ID(), 'category-product', array("fields" => "ids")), ',');
                    $args = array(
                        'post_type' => 'product',
                        'posts_per_page' => 3,
                        'post__not_in' => array(get_the_ID(), get_option('sticky_posts'),
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'category-product',
                                    'field' => 'id',
                                    'terms' => $category_list
                                )
                            ),
                        ),
                    );
                    $products_list = new WP_Query($args);
                    ?>
                    <?php if ($products_list->have_posts()) : ?>
                        <div class="block product-block">
                            <h2 class="title"><span><?php _e('Sản phẩm cùng loại', THEMENAME) ?></span><span class="border"></span></h2>
                            <div class="inner">
                                <?php
                                while ($products_list->have_posts()) : $products_list->the_post();
                                    $product = get_post();
                                    $price = $cfs->get('price', $product->ID);
                                    $image_product = $cfs->get('image_product', $product->ID);
                                    $image = wp_get_attachment_image_src($image_product[0]["image"], 'sanphammoi');
                                    ?>
                                    <div class="col-sm-6 col-md-4">
                                        <div class="thumbnail">
                                            <a href="<?php echo get_permalink($product->ID); ?>" title="<?php echo $product->post_title; ?>">
                                                <img src="<?php echo $image[0]; ?>" alt="<?php echo $product->post_title; ?>"/>
                                                <div class="caption">
                                                    <h3><?php echo $product->post_title; ?></h3>
                                                    <p class="price">$ <?php echo $price; ?> vnđ</p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endwhile; // end of the loop.  ?>
            </div>
        </div>
    </main>
</div>
<?php
get_footer();