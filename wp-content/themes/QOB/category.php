<?php
/**
 * The template for displaying Category pages
 *
 * Used to display archive-type pages for posts in a category.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
get_header();
?>
<div class="inner">
    <main id="main">
        <div class="container">
            <div class="highlight">
                <div class="block news-block">
                    <h2 class="title"><span><?php echo single_cat_title('', false); ?></span><span class="border"></span></h2>
                    <div class="row inner">
                        <div class="col-md-12">
                            <?php
                            // Start the Loop.
                            while (have_posts()) : the_post();
                                $post = get_post();
                                $image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'tintucnew');
                                ?>
                                <div class="media">
                                    <a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
                                        <div class="thumb pull-left media-object">
                                            <img src="<?php echo $image[0] ?>" alt="<?php the_title(); ?>"/>
                                        </div>
                                        <div class="media-body">
                                            <div class="desc">
                                                <h3 class="heading"><?php the_title(); ?></h3>
                                                <p><?php echo wp_trim_words($post->post_content, 50) ?></p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <?php
                            endwhile;
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
<?php get_footer(); ?>