<?php
/**
 * Template Name: Product Page
 */
get_header();
global $cfs;
$categories = get_terms(
        'category-product', array(
    'orderby' => 'name',
    'order' => 'ASC',
    'parent' => 0,
    'hide_empty' => 0
    )
);
?>
<div class="inner">
    <main id="main">
        <div class="container">
            <div class="highlight">
                <?php
                foreach ($categories as $category_product) :
                    if ($category_product->term_id != 18) :
                        $args = array(
                            'post_type' => 'product',
                            'posts_per_page' => 6,
                            'post_status' => 'publish',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'category-product',
                                    'field' => 'id',
                                    'terms' => $category_product->term_id
                                )
                            )
                        );
                        $product_in_category = get_posts($args);
                        ?>
                        <div class="block product-block">
                            <h2 class="title"><span><?php echo $category_product->name ?></span><span class="border"></span></h2>
                            <div class="row inner">
                                <?php
                                foreach ($product_in_category as $product) :
                                    $price = $cfs->get('price', $product->ID);
                                    $image_product = $cfs->get('image_product', $product->ID);
                                    $image = wp_get_attachment_image_src($image_product[0]["image"], 'sanphammoi');
                                    ?>
                                    <div class="col-sm-6 col-md-4">
                                        <div class="thumbnail">
                                            <a href="<?php echo get_permalink($product->ID); ?>" title="<?php echo $product->post_title ?>">
                                                <img src="<?php echo $image[0] ?>" alt="<?php echo $product->post_title ?>"/>
                                                <div class="caption">
                                                    <h3><?php echo $product->post_title ?></h3>
                                                    <p class="price">$ <?php echo $price ?> vnđ</p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <?php
                                endforeach;
                                ?>
                            </div>
                        </div>
                        <?php
                    endif;
                endforeach;
                ?>
            </div>
        </div>
    </main>
</div>
<?php get_footer(); ?>