<?php
/**
 * Template Name: Đại Lý Page
 */
get_header();
global $cfs;
$categories_daily = get_terms(
        'category-daily', array(
    'orderby' => 'name',
    'order' => 'ASC',
    'parent' => 0,
    'hide_empty' => 0
        )
);
?>
<main id="main">
    <div class="container">
        <div class="highlight">
            <?php
            while (have_posts()) : the_post();
                ?>
                <h2 class="title"><span><?php the_title() ?></span><span class="border"></span></h2>
                <div class="row inner">
                    <div id="accordion" class="panel-group col-md-12">
                        <?php
                        foreach ($categories_daily as $key_daily => $daily) :
                            $args = array(
                                'post_type' => 'daily',
                                'posts_per_page' => -1,
                                'post_status' => 'publish',
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => 'category-daily',
                                        'field' => 'id',
                                        'terms' => $daily->term_id
                                    )
                                )
                            );
                            $daily_in_category = get_posts($args);
                            ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#tab-<?php echo $key_daily + 1 ?>">
                                            <span class="icon-local"></span>
                                            <span class="text"><?php echo $daily->name ?></span>
                                            <span class="info"><?php _e('Thông tin shop', THEMENAME); ?></span>
                                        </a>
                                    </h4>
                                </div>
                                <div id="tab-<?php echo $key_daily + 1 ?>" class="panel-collapse collapse <?php echo $key_daily == 0 ? "in" : '' ?>">
                                    <div class="panel-body">
                                        <?php
                                        foreach ($daily_in_category as $key_daily_post => $daily_post) :
                                            $image = wp_get_attachment_image_src(get_post_thumbnail_id($daily_post->ID), 'daily');
                                            $address_daily = $cfs->get('dai_ly', $daily_post->ID);
                                            $chu_shop = $cfs->get('chu_shop', $daily_post->ID);
                                            $phone = $cfs->get('phone', $daily_post->ID);
                                            $facebook = $cfs->get('facebook', $daily_post->ID);
                                            $email = $cfs->get('email', $daily_post->ID);
                                            ?>
                                            <div class="thumbnail">
                                                <img src="<?php echo $image[0] ?>" alt="<?php echo $daily_post->post_title ?>"/>
                                                <div class="caption">
                                                    <ul class="list-unstyled">
                                                        <li class="odd">
                                                            <p><?php echo $address_daily ?></p>
                                                        </li>
                                                        <li class="even">
                                                            <p>Chủ shop:  <strong><?php echo $chu_shop ?></strong></p>
                                                        </li>
                                                        <li class="odd">
                                                            <p>Điện thoại: <a href="tel:<?php echo $phone ?>" title="phone"><?php echo $phone ?></a></p>
                                                        </li>
                                                        <li class="even">
                                                            <p>Face: <a href="<?php echo $facebook ?>" tagert="_blank" title="infor FB"><?php echo $facebook ?></a></p>
                                                        </li>
                                                        <li class="odd">
                                                            <p>Email: <a href="mailto:<?php echo $email ?>" title="mail"><?php echo $email ?></a></p>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <?php
                                        endforeach;
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                        endforeach;
                        ?>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
    </div>
</main>
<?php
get_footer();
