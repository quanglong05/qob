<?php
/**
 * Template Name: Contact Page
 */
get_header();
?>
<div class="inner">
    <main id="main">
        <div class="container">
            <div class="highlight">
                <div class="block block-1">
                    <h2 class="title"><span><?php _e('Chi tiết Tin tức', THEMENAME) ?></span><span class="border"></span></h2>
                    <div class="inner">
                        <div class="row">
                            <div class="block-detail">
                                <div class="desc col-md-12"></div>
                                <div class="col-md-5">
                                    <?php
                                    while (have_posts()) : the_post();
                                        ?>
                                        <?php the_content(); ?>
                                    <?php endwhile; ?>
                                    <?php echo apply_filters('the_content', '[contact-form-7 id="140" title="Contact form 1"]'); ?>
                                </div>
                                <div class="col-md-7">
                                    <div class="iframe"><?php echo apply_filters('the_content', '[wpgmza id="1"]'); ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
<?php
get_footer();