<?php
/**
 * The Footer Sidebar
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
if (!is_active_sidebar('sidebar-3')) {
    return;
}
?>
<div class="inner">
    <div class="container">
        <div class="row">
            <?php dynamic_sidebar('sidebar-3'); ?>
        </div>
    </div>
</div>