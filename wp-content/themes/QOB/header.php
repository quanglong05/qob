<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
    <!--<![endif]-->
    <head>
        <meta charset="<?php bloginfo('charset'); ?>" />
        <meta name="viewport" content="width=device-width" />
        <title><?php wp_title('|', true, 'right'); ?></title>
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
        <link href="<?php echo get_template_directory_uri(); ?>/bootstrap.css" rel="stylesheet">
        <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
        <script src="<?php echo get_template_directory_uri(); ?>/js/modernizr.js"></script>
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
        <div id="container">
            <header id="header">
                <nav id="nav-mid" role="navigation" class="navbar navbar-default">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" data-toggle="collapse" data-target="#nav-main" class="navbar-toggle"><span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                            </button>
                            <a href="<?php echo home_url(); ?>" title="home page" class="navbar-brand"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="Tour Phu Quoc"></a>
                        </div>
                        <div id="nav-main" class="collapse navbar-collapse">
                            <?php
                            $locations = get_nav_menu_locations();
//                            wp_nav_menu(array('menu' => $locations['primary'], 'menu_class' => 'nav navbar-nav', 'container' => '', 'walker' => new QOB_LBMenuWalker() ));
                            wp_nav_menu(array('menu' => $locations['primary'], 'menu_class' => 'nav navbar-nav', 'container' => ''));
                            ?>
                        </div>
                    </div>
                </nav>
            </header>
            <?php
            $banners = get_list_banners();
            ?>
            <div id="slideshow" data-ride="carousel" class="carousel slide">
                <div class="carousel-inner">
                    <?php
                    foreach ($banners as $key => $banner) :
                        $image = wp_get_attachment_image_src(get_post_thumbnail_id($banner->ID), 'banner');
                        ?>
                        <div class="item <?php echo $key==0 ? 'active' : ''; ?>">
                            <img src="<?php echo $image[0]; ?>" alt="<?php echo $banner->post_title; ?>">
                        </div>
                    <?php endforeach; ?>
                </div>
                <a href="#slideshow" data-slide="prev" class="left carousel-control"><span></span></a>
                <a href="#slideshow" data-slide="next" class="right carousel-control"><span></span></a>
            </div>