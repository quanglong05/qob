<?php

function get_list_banners()
{
    $args = array(
        'post_type' => 'banner',
        'posts_per_page' => -1,
        'post_status' => 'publish',
    );

    $banners_list = get_posts($args);
    return $banners_list;
}

function get_post_customer_experience()
{
    $args = array(
        'post_type' => 'customer_experience',
        'posts_per_page' => 1,
        'post_status' => 'publish',
    );

    $customer_experience_list = get_posts($args);
    return $customer_experience_list;
}

function get_post_tintuc()
{
    $args = array('posts_per_page' => 3, 'category' => 1);
    $tintuc_list = get_posts($args);
    return $tintuc_list;
}

function get_list_product_sanphammoi()
{
    $args = array(
        'post_type' => 'product',
        'post_status' => 'publish',
        'posts_per_page' => 9,
        'tax_query' => array(
            array(
                'taxonomy' => 'category-product',
                'field' => 'slug',
                'terms' => 'san-pham-moi'
            )
        )
    );
    $product_list = get_posts($args);
    return $product_list;
}

function contact_form_class_attr($class)
{
    $class = 'form';

    return $class;
}

add_filter('wpcf7_form_class_attr', 'contact_form_class_attr');
