<?php

add_action('init', 'sm_theme_register_taxonomies');
add_action('init', 'sm_theme_custom_post_type');

/**
 * Register taxonomies use in theme
 */
function sm_theme_register_taxonomies() {
    // Register taxonomy team group
    register_taxonomy('category-product', array('product'), array(
        'hierarchical' => true, // Hierarchical taxonomy (like categories)
        // This array of options controls the labels displayed in the WordPress Admin UI
        'labels' => array(
            'name' => _x('Categories Product', THEMENAME),
            'singular_name' => _x('Categories Product', THEMENAME),
            'search_items' => __('Search Category Product', THEMENAME),
            'all_items' => __('All Categories Product', THEMENAME),
            'parent_item' => __('Parent Category Product', THEMENAME),
            'parent_item_colon' => __('Parent Category Product:', THEMENAME),
            'edit_item' => __('Edit Category Product', THEMENAME),
            'update_item' => __('Update Category Product', THEMENAME),
            'add_new_item' => __('Add New Category Product', THEMENAME),
            'new_item_name' => __('New Report Category Product', THEMENAME),
            'menu_name' => __('Categories Product', THEMENAME),
        ),
        // Control the slugs used for this taxonomy
        'rewrite' => array(
            'slug' => 'category-product', // This controls the base slug that will display before each term
            'with_front' => false, // Don't display the category base before "/locations/"
            'hierarchical' => true, // This will allow URL's like "/locations/boston/cambridge/"
        ),
            )
    );
    // Register taxonomy team group
    register_taxonomy('category-daily', array('daily'), array(
        'hierarchical' => true, // Hierarchical taxonomy (like categories)
        // This array of options controls the labels displayed in the WordPress Admin UI
        'labels' => array(
            'name' => _x('Categories Store', THEMENAME),
            'singular_name' => _x('Categories Store', THEMENAME),
            'search_items' => __('Search Category Store', THEMENAME),
            'all_items' => __('All Categories Store', THEMENAME),
            'parent_item' => __('Parent Category Store', THEMENAME),
            'parent_item_colon' => __('Parent Category Store:', THEMENAME),
            'edit_item' => __('Edit Category Store', THEMENAME),
            'update_item' => __('Update Category Store', THEMENAME),
            'add_new_item' => __('Add New Category Store', THEMENAME),
            'new_item_name' => __('New Report Category Store', THEMENAME),
            'menu_name' => __('Categories Store', THEMENAME),
        ),
        // Control the slugs used for this taxonomy
        'rewrite' => array(
            'slug' => 'category-daily', // This controls the base slug that will display before each term
            'with_front' => false, // Don't display the category base before "/locations/"
            'hierarchical' => true, // This will allow URL's like "/locations/boston/cambridge/"
        ),
            )
    );
}

/**
 * Register a custom post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function sm_theme_custom_post_type() {
    /**
     * Register a banner post type.
     */
    $banner_labels = array(
        'name' => _x('Banners', 'post type general name'),
        'singular_name' => _x('Banner', 'post type singular name'),
        'menu_name' => _x('Banners', 'admin menu'),
        'name_admin_bar' => _x('Banner', 'add new on admin bar'),
        'add_new' => _x('Add New', 'book'),
        'add_new_item' => __('Add New Banner'),
        'new_item' => __('New Banner'),
        'edit_item' => __('Edit Banner'),
        'view_item' => __('View Banner'),
        'all_items' => __('All Banners'),
        'search_items' => __('Search Banner'),
        'parent_item_colon' => __('Parent Banner:'),
        'not_found' => __('No Banner found.'),
        'not_found_in_trash' => __('No Banner found in Trash.'),
    );

    $banner_args = array(
        'labels' => $banner_labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'banner'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title', 'page-attributes')
    );

    register_post_type('banner', $banner_args);


    /**
     * Register a blog post type.
     */
    $product_labels = array(
        'name' => _x('Products', 'post type general name'),
        'singular_name' => _x('Products', 'post type singular name'),
        'menu_name' => _x('Products', 'admin menu'),
        'name_admin_bar' => _x('Products', 'add new on admin bar'),
        'add_new' => _x('Add New', 'book'),
        'add_new_item' => __('Add New Product'),
        'new_item' => __('New Product'),
        'edit_item' => __('Edit Product'),
        'view_item' => __('View Product'),
        'all_items' => __('All Products'),
        'search_items' => __('Search Product'),
        'parent_item_colon' => __('Parent Product:'),
        'not_found' => __('No Product found.'),
        'not_found_in_trash' => __('No Product found in Trash.'),
    );

    $product_args = array(
        'labels' => $product_labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'product'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title', 'editor', 'page-attributes', 'comments')
    );

    register_post_type('product', $product_args);

    /**
     * Register a customer_experience post type.
     */
    $customer_experience_labels = array(
        'name' => _x('Customer Experience', 'post type general name'),
        'singular_name' => _x('Customer Experience', 'post type singular name'),
        'menu_name' => _x('Customer Experience', 'admin menu'),
        'name_admin_bar' => _x('Customer Experience', 'add new on admin bar'),
        'add_new' => _x('Add New', 'book'),
        'add_new_item' => __('Add New Customer Experience'),
        'new_item' => __('New Customer Experience'),
        'edit_item' => __('Edit Customer Experience'),
        'view_item' => __('View Customer Experience'),
        'all_items' => __('All Customer Experience'),
        'search_items' => __('Search Customer Experience'),
        'parent_item_colon' => __('Parent Customer Experience:'),
        'not_found' => __('No Customer Experience found.'),
        'not_found_in_trash' => __('No Customer Experience found in Trash.'),
    );

    $customer_experience_args = array(
        'labels' => $customer_experience_labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'customer_experience'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title', 'editor', 'page-attributes', 'comments')
    );

    register_post_type('customer_experience', $customer_experience_args);

    /**
     * Register a customer_experience post type.
     */
    $daily_labels = array(
        'name' => _x('Stores', 'post type general name'),
        'singular_name' => _x('Stores', 'post type singular name'),
        'menu_name' => _x('Stores', 'admin menu'),
        'name_admin_bar' => _x('Stores', 'add new on admin bar'),
        'add_new' => _x('Add New', 'book'),
        'add_new_item' => __('Add New Store'),
        'new_item' => __('New Store'),
        'edit_item' => __('Edit Store'),
        'view_item' => __('View Store'),
        'all_items' => __('All Stores'),
        'search_items' => __('Search Store'),
        'parent_item_colon' => __('Parent Store:'),
        'not_found' => __('No Store found.'),
        'not_found_in_trash' => __('No Store found in Trash.'),
    );

    $daily_args = array(
        'labels' => $daily_labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'daily'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title', 'thumbnail')
    );

    register_post_type('daily', $daily_args);
}
