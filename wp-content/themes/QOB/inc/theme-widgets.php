<?php
/**
 * Text widget class
 *
 * @since 2.8.0
 */
class BOQ_Widget_Address extends WP_Widget
{

    function __construct()
    {
        $widget_ops = array('classname' => 'widget_text', 'description' => __('Arbitrary text or HTML.'));
        parent::__construct('address', __('Address Jobskills'), $widget_ops, $control_ops);
    }

    function widget($args, $instance)
    {
        extract($args);

        /** This filter is documented in wp-includes/default-widgets.php */
        $title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title'], $instance, $this->id_base);

        /**
         * Filter the content of the Text widget.
         *
         * @since 2.3.0
         *
         * @param string    $widget_text The widget content.
         * @param WP_Widget $instance    WP_Widget instance.
         */
        $home = apply_filters('widget_home', empty($instance['home']) ? '' : $instance['home'], $instance);
        $phone = apply_filters('widget_phone', empty($instance['phone']) ? '' : $instance['phone'], $instance);
        $email = apply_filters('widget_email', empty($instance['email']) ? '' : $instance['email'], $instance);
        $text1 = apply_filters('widget_text1', empty($instance['text1']) ? '' : $instance['text1'], $instance);
        $text2 = apply_filters('widget_text2', empty($instance['text2']) ? '' : $instance['text2'], $instance);
        echo $before_widget;
        if (!empty($title)) {
            echo $before_title . $title . $after_title;
        }
        ?>
        <ul class="list-unstyled">
            <li><i class="glyphicon glyphicon-map-marker"></i><p><?php echo $home; ?></p></li>
            <li><i class="glyphicon glyphicon-earphone"></i><p><?php _e( 'Phone', THEMENAME ) ?>: <a title="phone" href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a></p></li>
            <li><i class="glyphicon glyphicon-envelope"></i><p><?php _e( 'Email', THEMENAME ) ?>: <a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></p></li>
            <li><p><?php echo $text1; ?></p></li>
            <li><p><?php echo $text2; ?></p></li>
        </ul>
        <?php
        echo $after_widget;
    }

    function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['home'] = strip_tags($new_instance['home']);
        $instance['phone'] = strip_tags($new_instance['phone']);
        $instance['email'] = strip_tags($new_instance['email']);
        $instance['text1'] = strip_tags($new_instance['text1']);
        $instance['text2'] = strip_tags($new_instance['text2']);
        return $instance;
    }

    function form($instance)
    {
        $instance = wp_parse_args((array) $instance, array('title' => ''));
        $title = strip_tags($instance['title']);
        $home = esc_textarea($instance['home']);
        $phone = esc_textarea($instance['phone']);
        $email = esc_textarea($instance['email']);
        $text1 = esc_textarea($instance['text1']);
        $text2 = esc_textarea($instance['text2']);
        ?>
        <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>
        <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Home:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('home'); ?>" name="<?php echo $this->get_field_name('home'); ?>" type="text" value="<?php echo esc_attr($home); ?>" /></p>
        <p><label for="<?php echo $this->get_field_id('phone'); ?>"><?php _e('Phone:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('phone'); ?>" name="<?php echo $this->get_field_name('phone'); ?>" type="text" value="<?php echo esc_attr($phone); ?>" /></p>
        <p><label for="<?php echo $this->get_field_id('email'); ?>"><?php _e('Email:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('email'); ?>" name="<?php echo $this->get_field_name('email'); ?>" type="text" value="<?php echo esc_attr($email); ?>" /></p>
        <p><label for="<?php echo $this->get_field_id('text1'); ?>"><?php _e('Text 1:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('text1'); ?>" name="<?php echo $this->get_field_name('text1'); ?>" type="text" value="<?php echo esc_attr($text1); ?>" /></p>
        <p><label for="<?php echo $this->get_field_id('text2'); ?>"><?php _e('Text 2:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('text2'); ?>" name="<?php echo $this->get_field_name('text2'); ?>" type="text" value="<?php echo esc_attr($text2); ?>" /></p>
        <?php
    }

}