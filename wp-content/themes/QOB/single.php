<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
get_header();
global $dynamic_featured_image, $cfs;
?>

<div class="inner">
    <main id="main">
        <div class="container">
            <div class="highlight">
                <div class="block news-block">
                    <h2 class="title"><span><?php _e('Chi tiết Tin tức', THEMNAME); ?></span><span class="border"></span></h2>
                    <div class="row inner">
                        <?php
                        // Start the Loop.
                        while (have_posts()) : the_post();
                            ?>
                            <div class="col-md-12">
                                <h3><?php the_title(); ?></h3>
                                <div class="desc">
                                    <?php the_content(); ?>
                                </div>
                            </div>
                            <div class="col-md-12 comment-block">
                                <?php comments_template(); ?>
                            </div>
                            <?php
                        endwhile;
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
<?php get_footer(); ?>