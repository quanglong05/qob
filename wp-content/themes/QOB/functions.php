<?php

/**
 * BOQ functions and definitions
 *
 * Sets up the theme and provides some helper functions, which are used
 * in the theme as custom template tags. Others are attached to action and
 * filter hooks in WordPress to change core functionality.
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development and
 * http://codex.wordpress.org/Child_Themes), you can override certain functions
 * (those wrapped in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before the parent
 * theme's file, so the child theme functions would be used.
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are instead attached
 * to a filter or action hook.
 *
 * For more information on hooks, actions, and filters, @link http://codex.wordpress.org/Plugin_API
 */
define('THEMENAME', 'QOB');
define('THEMEDIR', get_template_directory());

/**
 * QOB setup.
 *
 * Sets up theme defaults and registers the various WordPress features that
 * Twenty Twelve supports.
 *
 * @uses load_theme_textdomain() For translation/localization support.
 * @uses add_editor_style() To add a Visual Editor stylesheet.
 * @uses add_theme_support() To add support for post thumbnails, automatic feed links,
 * 	custom background, and post formats.
 * @uses register_nav_menu() To add support for navigation menus.
 * @uses set_post_thumbnail_size() To set a custom post thumbnail size.
 */
function QOB_setup() {
    /*
     * Makes Twenty Twelve available for translation.
     *
     * Translations can be added to the /languages/ directory.
     * If you're building a theme based on Twenty Twelve, use a find and replace
     * to change 'twentytwelve' to the name of your theme in all the template files.
     */
    load_theme_textdomain(THEMENAME, get_template_directory() . '/languages');

    // Adds RSS feed links to <head> for posts and comments.
    add_theme_support('automatic-feed-links');

    // This theme supports a variety of post formats.
    add_theme_support('post-formats', array('aside', 'image', 'link', 'quote', 'status'));

    // This theme uses wp_nav_menu() in one location.
    register_nav_menu('primary', __('Primary Menu', THEMENAME));
    register_nav_menu('footer', __('Footer Menu', THEMENAME));

    // This theme uses a custom image size for featured images, displayed on "standard" posts.
    add_theme_support('post-thumbnails');
    set_post_thumbnail_size(624, 9999); // Unlimited height, soft crop
    add_image_size('banner', 1200, 400, true);
    add_image_size('sanphammoi', 280, 190, true);
    add_image_size('tintucnew', 177, 107, true);
    add_image_size('page_left', 555, 407, true);
    add_image_size('page_right', 252, 266, true);
    add_image_size('product_detail', 550, 375, true);
    add_image_size('daily', 390, 230, true);
}

add_action('after_setup_theme', 'QOB_setup');

/**
 * Add class for body class
 */
function custom_body_class($classes) {
    global $post;
    $classes = array();
    if (is_front_page()) {
        $classes[] = "home";
    }
    if (is_page()) {
        if ($post->ID == 53) {
            $classes[] = "page-1 daily-detail";
        } else if ($post->ID == 159) {
            $classes[] = "page-1 product-page";
        } else if ($post->ID == 11) {
            $classes[] = "page-1 contact";
        } else {
            $classes[] = "page-1 about-detail";
        }
    }
    if (is_category()) {
        $classes[] = "page-1 news-page";
    }
    if ($post->post_type == 'product') {
        $classes[] = "page-1 detail-page";
    }
    if ($post->post_type == 'post') {
        $classes[] = "page-1 news-page";
    }
    return $classes;
}

add_filter('body_class', 'custom_body_class');

/**
 * Register three Twenty Fourteen widget areas.
 *
 * @since Twenty Fourteen 1.0
 */
function BOQ_widgets_init() {
    require get_template_directory() . '/inc/theme-widgets.php';
    register_widget('BOQ_Widget_Address');

    register_sidebar(array(
        'name' => __('Primary Sidebar', THEMENAME),
        'id' => 'sidebar-1',
        'description' => __('Main sidebar that appears on the left.', THEMENAME),
        'before_widget' => '<div class="col-md-4">',
        'after_widget' => '</div>',
        'before_title' => '<h2>',
        'after_title' => '</h2>',
    ));
    register_sidebar(array(
        'name' => __('Content Sidebar', THEMENAME),
        'id' => 'sidebar-2',
        'description' => __('Additional sidebar that appears on the right.', THEMENAME),
        'before_widget' => '<div class="col-md-4">',
        'after_widget' => '</div>',
        'before_title' => '<h2>',
        'after_title' => '</h2>',
    ));
    register_sidebar(array(
        'name' => __('Footer Widget Area', THEMENAME),
        'id' => 'sidebar-3',
        'description' => __('Appears in the footer section of the site.', THEMENAME),
        'before_widget' => '<div class="col-md-4">',
        'after_widget' => '</div>',
        'before_title' => '<h2>',
        'after_title' => '</h2>',
    ));
}

add_action('widgets_init', 'BOQ_widgets_init');

require_once get_template_directory() . '/inc/theme-util.php';
require_once get_template_directory() . '/inc/post-type.php';
require_once get_template_directory() . '/class/QOB-LBMenuWalker.php';
